class BitbucketService
  def service_enabled?
    Setting.plugin_redmine_bitbucket[:service_enabled]
  end

  def valid_key?(key)
    setting_key = Setting.plugin_redmine_bitbucket[:service_key]
    return true if setting_key.to_s==''
    key == setting_key
  end

  def find_project(project_id)
    scope = Project.active.has_module(:repository)
    project = scope.find_by_identifier(project_id.downcase)
    raise ActiveRecord::RecordNotFound unless project
    project
  end

  def find_and_update_repository(project_id, payload)
    project = find_project(project_id)
    adapter = BitbucketAdapter.new(payload)
    repository = project.repositories.find_by_identifier(adapter.identifier)

    if repository
      Resque.enqueue(BitbucketWorker, 'update', project_id, payload)

    elsif Setting.plugin_redmine_bitbucket[:auto_create]
      Resque.enqueue(BitbucketWorker, 'create', project_id, payload)

    else
      raise ActiveRecord::RecordNotFound
    end

    true
  end
end
