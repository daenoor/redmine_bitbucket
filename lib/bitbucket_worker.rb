class BitbucketWorker
  @queue = :bitbucket

  def self.perform(action, project_id, payload)
    project = find_project(project_id)
    adapter = BitbucketAdapter.new(payload)

    if action == 'create'
      repository = adapter.create_repository(project)
    elsif action == 'update'
      repository = project.repositories.find_by_identifier(adapter.identifier)
      adapter.update_repository(repository)
    else
      raise ArgumentError "action should be either create or update, got #{action}"
    end

    repository.fetch_changesets
  end

  def self.find_project(project_id)
    scope = Project.active.has_module(:repository)
    project = scope.find_by_identifier(project_id.downcase)
    raise ActiveRecord::RecordNotFound unless project
    project
  end

end