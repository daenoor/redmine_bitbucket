require "json"

class BitbucketServiceController < ApplicationController
  unloadable

  skip_before_filter :verify_authenticity_token, :check_if_login_required

  def index
    bitbucket = BitbucketService.new

    unless bitbucket.service_enabled? && bitbucket.valid_key?(params[:key])
      return render :nothing => true, :status => 404      
    end

    result = bitbucket.find_and_update_repository(params[:project_id], params[:payload])

    if result.nil?
      logger.debug { "BitbucketPlugin: Invalid repository" }
      return render :nothing => true, :status => 500
    end

    render :nothing => true, :status => 200
  rescue ActiveRecord::RecordNotFound
    logger.debug { "BitbucketPlugin: Record not found" }
    render :nothing => true, :status => 404
  end
end
