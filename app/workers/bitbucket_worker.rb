class BitbucketWorker

  include Sidekiq::Worker

  def create_and_fetch(adapter, project)
    repository = adapter.create_repository(project)
    repository.fetch_changesets
  end

  def update_and_fetch(adapter, repository)
    adapter.update_repository(repository)
    repository.fetch_changesets
  end
end
